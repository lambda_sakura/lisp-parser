(in-package :cl-user)
(defpackage lisp-parser-test
  (:use :cl
        :lisp-parser
        :prove))
(in-package :lisp-parser-test)

;; NOTE: To run this test file, execute `(asdf:test-system :lisp-parser)' in your Lisp.

(plan nil)

;; blah blah blah.

(finalize)
