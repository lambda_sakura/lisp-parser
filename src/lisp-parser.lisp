(in-package :cl-user)
(defpackage lisp-parser
  (:use :cl :optima :mpc :mpc.characters :mpc.numerals :alexandria))
(in-package :lisp-parser)

(import 'alexandria:curry)
(import 'alexandria:plist-hash-table)

(defun merge-hash-tables (&rest tables)
  (let ((union
         (make-hash-table
          :test (first
                 (sort (mapcar #'hash-table-test tables) #'>
                       :key (lambda (test)
                              (ecase test
                                (eq 0)
                                (eql 1)
                                (equal 2)
                                (equalp 3)))))
          :size (reduce #'max (mapcar #'hash-table-size tables)))))
    (dolist (table tables)
      (maphash (lambda (key val) (setf (gethash key union) val)) table))
    union))

;;
;; Lisp Object
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defstruct (latom (:constructor latom (a))) (a))
(defstruct (llist (:constructor llist (l))) (l))
(defstruct (ldotted-list (:constructor ldotted-list (head tail))) head tail)
(defstruct (lstring (:constructor lstring (s))) (s))
(defstruct (lcharacter (:constructor lcharacter (c))) c)
(defstruct (lbool (:constructor lbool (b))) b)
(defstruct (lnumber (:constructor lnumber (n))) (n))
(defstruct (lprimitive-func (:constructor lprimitive-func (val))) val)
(defstruct (lfunc (:constructor lfunc (params vararg body closure))) params vararg body closure)
(defstruct (lio-func (:constructor lio-func (v))) v)
(defstruct (lport (:constructor lport (handle))) handle)

(defmethod unwords-list ((l list))
  (format nil "~{ ~A ~}" (mapcar (lambda (x) (show-val x)) l)))
(defmethod show-val ((a latom)) (latom-a a))
(defmethod show-val ((s lstring)) (format nil "\"~A\"" (lstring-s s)))
(defmethod show-val ((s lnumber)) (format nil "\"~A\"" (lnumber-n n)))
(defmethod show-val ((n lnumber)) (format nil "~A" (lnumber-n n)))
(defmethod show-val ((b lbool)) (if (lbool-b b) "#T" "#F"))
(defmethod show-val ((l llist))
  (optima:match l
    ((llist :l x) (format nil "(~A)" (unwords-list x)))))
(defmethod show-val ((l ldotted-list))
  (optima:match l
    ((ldotted-list :head x :tail v) (format nil "(~A . ~A)" (unwords-list x) (show-val v)))))
(defmethod show-val ((f lfunc)) (format nil "#Function ~A" (lfunc-params f)))


;;
;; Unpacker
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; Extract Data from AST
(defmethod unpack-num ((l lstring)) (parse-integer (lstring-s l)))
(defmethod unpack-num ((n lnumber)) (lnumber-n n))
(defmethod unpack-num ((l llist))
  (optima:match (llist-l l) ((list x) (unpack-num x))))

(defmethod unpack-str ((l lstring)) (lstring-s l))
(defmethod unpack-str ((n lnumber)) (prin1-to-string (lnumber-n n)))
(defmethod unpack-str ((b lbool)) (prin1-to-string (lbool-b b)))
(defmethod unpack-bool ((b lbool)) (prin1-to-string (lbool-b b)))

(defun bool-bin-op (unpacker op args)
  (if (equal (length args) 2)
      (let ((left (apply unpacker (first args)))
            (right (apply unpacker (second args))))
        (lbool (funcall op left right)))))

(defun numeric-bin-op (op params)
  (lnumber (reduce op (mapcar #'unpack-num params))))

(defun num-bool-bin-op (args)
  (bool-bin-op unpack-num args))

(defun str-bool-bin-op (args)
  (bool-bin-op unpack-str args))

(defun bool-bool-bin-op (args)
  (bool-bin-op unpack-bool args))
;;
;; Condition
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(define-condition invalid (error)
  ((message :initarg :message :reader message)
   (value :initarg :value :reader value)))

(define-condition type-missmatch (error)
  ((message :initarg :message :reader message)
   (value :initarg :value :reader value)))

(define-condition parser (error)
  ((message :initarg :message :reader message)
   (value :initarg :value :reader value)))

(define-condition bad-special-form (error)
  ((message :initarg :message :reader message)
   (value :initarg :value :reader value)))

(define-condition not-function (error)
  ((message :initarg :message :reader message)
   (value :initarg :value :reader value)))

(define-condition unbound-var (error)
  ((message :initarg :message :reader message)
   (value :initarg :value :reader value)))

(define-condition default (error)
  ((message :initarg :message :reader message)
   (value :initarg :value :reader value)))

;;
;; Parser Functions
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun =symbol () (=one-of "!$%&|*+-/:<=>?@^_~"))
(defun =spaces () (=zero-or-more (=one-of " \t\n\r")))
(defun =alpha () (=satisfies #'alpha-char-p))
(defun =letter () (=alpha))
(defun =alpha-num () (=satisfies #'alphanumericp))

(defun =hex-digit () (=one-of "0123456789ABCDEF"))
(defun =oct-digit () (=one-of "01234567"))
(defun =not-followed (parser) (=if parser (=fail) (=result "")))

(defun parse-expr ()
  (=or (parse-atom)
       (parse-string)
       (=plus (parse-number)
              (parse-bool)
              (parse-character))
       (parse-quoted)
       (=let* ((_ (=character #\())
               (x (=plus (parse-dotted-list)
                         (parse-list)
                         ))
               (_ (=character #\))))
         (=result x))))


(defun escaped-chars ()
  (=let* ((_ (=character #\\))
          (x (=one-of "\\\"nrt")))
    (=result (cond ((char= x #\n) #\newline)
                   ((char= x #\r) #\newline)
                   ((char= x #\t) #\t)
                   (t x)))))
(defun parse-character ()
  (=let* ((_ (=plus (=string "#\\")))
          (value (=plus (=or (=string "newline" nil)
                             (=string "space" nil))
                        (=prog1 (=alpha)
                                (=not-followed (=alpha-num))))))
    (=result (lcharacter value))))
(defun parse-string ()
  (=let* ((_ (=character #\"))
          (x (=zero-or-more (=or (escaped-chars)
                                 (=none-of "\"\\"))))
          (_ (=character #\")))
    (=result (lstring (coerce x 'string)))))

(defun parse-bool ()
  (=let* ((_ (=string "#"))
          (x (=one-of "tf")))
    (=result (if (string= "t" x) (lbool t) (lbool nil)))))

(defun parse-atom ()
  (=let* ((first  (=or (=letter) (=symbol)))
          (rest (=zero-or-more (=or (=letter) (=digit) (=symbol)))))
    (=result (latom (concatenate 'string
                                 (string first)
                                 (coerce rest 'string))))))

;;; parse numbers
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun parse-number ()
  (=or (parse-digital1)
       (parse-digital2)
       (parse-hex)
       (parse-oct)
       (parse-bin)))

(defun parse-decimal ()
  (=or (parse-digital1)
       (parse-digital2)))

(defun parse-digital1 ()
  (=let* ((num (=one-or-more (=digit))))
    (=result (lnumber (parse-integer (coerce num 'string))))))

(defun parse-digital2 ()
  (=let* ((_   (=plus (=string "#d")))
          (num (=one-or-more (=digit))))
    (=result (lnumber (parse-integer (coerce num 'string))))))

(defun parse-hex ()
  (=let* ((_ (=plus (=string "#x")))
          (x (=one-or-more (=hex-digit))))
    (=result (lnumber (parse-integer (coerce x 'string) :radix 16)))))

(defun parse-oct ()
  (=let* ((_ (=plus (=string "#o")))
          (x (=one-or-more (=oct-digit))))
    (=result (lnumber (parse-integer (coerce x 'string) :radix 8)))))

(defun parse-bin ()
  (=let* ((_ (=plus (=string "#b")))
          (x (=one-or-more (=one-of "10"))))
    (=result (lnumber (parse-integer (coerce x 'string) :radix 2)))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; parse list
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun =sep-by (p sep)
  (=or (=sep-by-one p sep)
       (=result '())))

(defun =sep-by-one (p sep)
  (=let* ((x p)
          (xs (=zero-or-more (=and sep p))))
    (=result (cons x xs))))

(defun =end-by (p sep)
  (=zero-or-more
   (=let* ((x p)
           (_ sep))
     (=result x))))

(defun parse-list ()
  (=let* ((x (=sep-by (parse-expr) (=spaces)))
          (- (=spaces)))
    (=result (llist x))))

(defun parse-dotted-list ()
  (=let* ((head (=end-by (parse-expr) (=spaces)))
          (tail (=and (=one-of ".") (=spaces) (parse-expr))))
    (=result (ldotted-list (car head) tail))))

(defun parse-quoted ()
  (=let* ((_ (=character #\'))
          (x (parse-expr)))
    (=result (llist (list (latom "quote") x)))))

;;
;; primitive functions
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun lisp-car (lst)
  (let ((temp (car lst)))
    (optima:match temp
      ((llist l) (car l))
      ((ldotted-list head tail) head))))

(defun lisp-cdr (lst)
  (optima:match lst
    ((list (ldotted-list head tail)) (llist (list tail)))
    ((list (llist l)) (llist (cdr l)))))

(defmethod lisp-cons ())
(defmethod lisp-eqv ())
(defmethod lisp-equal ())

;;
;; environment
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defparameter *io-primitives* '())
(defparameter *primitives*
  (list (cons "+" (curry #'numeric-bin-op #'+))
        (cons "-" (curry #'numeric-bin-op #'-))
        (cons "*" (curry #'numeric-bin-op #'*))
        (cons "/" (curry #'numeric-bin-op #'/))
        (cons "mod" (curry #'numeric-bin-op #'mod))
        (cons "="  (curry #'num-bool-bin-op #'=))
        (cons "<"  (curry #'num-bool-bin-op #'<))
        (cons ">"  (curry #'num-bool-bin-op #'>))
        (cons "/=" (curry #'num-bool-bin-op #'/=))
        (cons ">=" (curry #'num-bool-bin-op #'>=))
        (cons "<=" (curry #'num-bool-bin-op #'<=))
        (cons "string="  (curry #'str-bool-bin-op #'string=))
        (cons "string<"  (curry #'str-bool-bin-op #'string<))
        (cons "string>"  (curry #'str-bool-bin-op #'string>))
        (cons "string<="  (curry #'str-bool-bin-op #'string<=))
        (cons "string>="  (curry #'str-bool-bin-op #'string>=))
        (cons "car"  #'lisp-car)
        (cons "cdr"  #'lisp-cdr)
        (cons "cons"  #'lisp-cons)
        (cons "eq"  #'lisp-eqv)
        (cons "equal"  #'lisp-equal)))

(defun null-env () (make-hash-table :test #'equal))
(defun bound-p (env var) (gethash var env))
(defun set-var (env var lisp-val) (setf (gethash var env) lisp-val))
(defun get-var (env var) (gethash var env))
(defun define-var (env var lisp-val)
  (setf (gethash var env) lisp-val))

(defun bind-vars (env list)
  "create new environment"
  (let ((new-env (copy-hash-table env)))
    (merge-hash-tables new-env
                       (plist-hash-table list :test #'equal))))

(defun primitive-func-from-list (lst)
  (loop for i in lst
     append (list (car i) (lprimitive-func (cdr i)))))

(defun primitive-bindings ()
  (let ((primitives (primitive-func-from-list *primitives*)))
    (bind-vars (null-env) primitives)))

(defun make-func (varargs env params body)
  (lfunc params varargs body env))

(defun make-normal-func (env params body)
  (make-func nil env params body))

(defun make-var-args (vargs env params body)
  (make-func (show-val varargs) env params body))


;; eval
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defmethod lisp-eval (env (s lstring)) s)
(defmethod lisp-eval (env (c lcharacter)) c)
(defmethod lisp-eval (env (n lnumber)) n)
(defmethod lisp-eval (env (b lbool)) b)
(defmethod lisp-eval (env (a latom)) (get-var env (latom-a a)))

(defmethod lisp-eval (env (l llist))
  (optima:match l
    ;; parse quote
    ((llist :l (cons (latom :a "quote") val)) (car val))

    ;; parse if
    ((llist :l (list (latom :a "if") pred conseq alt))
     (let ((result (lbool-b (lisp-eval env pred))))
       (cond ((eq result t) (lisp-eval env conseq))
             ((eq result nil) (lisp-eval env alt)))))

    ;; parse set!
    ((llist :l (list (latom :a "set!") (latom :a var) form))
     (set-var env var (lisp-eval env form)))

    ;; parse define
    ((llist :l (list* (latom :a "define")
                      (latom :a var)
                      form))
     (define-var env var (lisp-eval env form)))

    ((llist :l (list* (latom :a "define")
                      (llist :l (list* (latom :a var) params))
                      body))
     (define-var env var (make-normal-func env params body)))

    ((llist :l (list* (latom :a "define")
                      (ldotted-list :head (list* (latom :a var) params) :tail varargs)
                      body))
     (define-var env var (make-var-args varargs env params body)))

    ;; parse lambda
    ((llist :l (list* (latom :a "lambda")
                      (llist :l (list params))
                      body))
     (make-normal-func env params body))

    ((llist :l (list* (latom :a "lambda")
                      (ldotted-list :head params  :tail varargs)
                      body))
     (make-var-args varargs env params body))

    ((llist :l (list* (latom :a "lambda")
                      varargs
                      body))
     (make-var-args varargs env '() body))

    ((llist :l lst)
     (let ((func (lisp-eval env (car lst)))
           (arg-vals (mapcar #'(lambda (x) (lisp-eval env x)) (cdr lst))))
       (lisp-apply func arg-vals)))))

(defmethod lisp-apply ((func lprimitive-func) args)
  (funcall (lprimitive-func-val func) args))

(defmethod lisp-apply ((func lfunc) args)
  (let ((params (mapcar #'(lambda (x) (show-val x)) (lfunc-params func)))
        (varargs (lfunc-vararg func))
        (body (lfunc-body func ))
        (closure (lfunc-closure func)))
    (if (or (equal (length params) (length args)) (not (null varargs)))
        (let ((new-env (bind-vars closure (mapcan #'list params args))))
          (car (last (mapcar #'(lambda (x) (lisp-eval new-env x)) body)))))))

;; Entry Point
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun read-or-throw (parser input-source)
  (let ((parser-with-handler (=handler-case
                              parser
                              (error (c) (make-condition 'parser :message c)))))
    (run parser-with-handler input-source)))

(defun read-expr (input-source)
  (read-or-throw (parse-expr) input-source))

(defun flush-str (str) (format t "~A" str))

(defun read-prompt (prompt)
  (flush-str prompt)
  (read-line *standard-input* ))

(defun eval-and-print (env expr)
  (format t "~A~%" (eval-string env expr)))

(defun eval-string (env expr)
  (show-val (lisp-eval env (read-expr expr))))
  ;; (handler-case (show-val (lisp-eval env (read-expr expr)))
  ;;   (t ())))

(defun run-repl ()
  (let ((env (primitive-bindings)))
    (loop for line = (read-prompt "Lisp>>> ")
       until (equal line "quit")
       do (progn (eval-and-print env line)))))
