#|
  This file is a part of lisp-parser project.
|#

(in-package :cl-user)
(defpackage lisp-parser-test-asd
  (:use :cl :asdf))
(in-package :lisp-parser-test-asd)

(defsystem lisp-parser-test
  :author ""
  :license ""
  :depends-on (:lisp-parser
               :prove)
  :components ((:module "t"
                :components
                ((:test-file "lisp-parser"))))

  :defsystem-depends-on (:prove-asdf)
  :perform (test-op :after (op c)
                    (funcall (intern #.(string :run-test-system) :prove-asdf) c)
                    (asdf:clear-system c)))
